The Docker Files and Docker-Compose files needed to build and run the images for the Django, Spring Boot, and MySQL sections of the code 
are here.

To build the images, the command "$ docker build -t name ." is used, where -t adds a tag, named "name", to the image. The "." specifies 
the location of the dockerfile. On a high level, the Django and Spring Boot sections of the code uses a Dockerfile to build the image. 
Then, the images can either be run seperately, using the "$ docker run -p host port: container port imagename" command, or they can be 
run by a docker compose file. More details on each Dockerfile are written below. The MySQL code also uses a Dockerfile to build the image, 
and a docker compose file is needed to implement a bind mount.

NOTE: TO USE THESE FILES, dockerfiles "Django_Dockerfile, MySQL_Dockerfile, SpringBoot_Dockerfile" must be renamed to "Dockerfile" and 
placed in the correct directories in their respective directories to build the images properly. This is because the Dockerfile copies 
over certain files in the current working directory on the Host into the container. Those files are later executed when the image is running. 
For Django, that would be manage.py, for SpringBoot, it would be the executable jar file from gradle, and for MySQL, it would be the dumped 
database. Also, MySQL_docker-compose.yml is used to run just the MySQL image, and docker-compose.yml runs all images (Djang, Spring, MySQL) 
together. To use MySQL_docker-compose.yml, it also must be renamed to docker-compose.yml. The location does not matter, as long as it is not 
in the same location as the larger docker-compose.yml file (used to run all 3 images at once). More details for each file are below:

1. Django
-Django_Dockerfile must be placed in the same directory as the manage.py file, and be renamed to Dockerfile.

2. SpringBoot
-SpringBoot_Dockerfile must be placed in the same directory as the DataReduction-1.0.jar file, which is in 
 /filtering/DataReduction/build/libs, and renamed to Dockerfile

3. MySQL
-The dumped database "runnerbuddydev2018-06-18.sql," MySQL_Dockerfile, MySQL_docker-compose.yml files must be in the same directory. 
 Also, MySQL_Dockerfile should be renamed to Dockerfile, and MySQL_docker-compose.yml should be renamed to docker-compose.yml.

4. docker-compose-yml
-Location does not matter, since the images are built before the compose file is executed.
-HOWEVER: it is good to double check the ports that are used for all three services. Under "ports:", two numbers are used, "number1:number2." Number1 refers to the Host's port, and number2 refers to the container's port. The container's port is exposed in the Dockerfile and should not be changed. However, number1, the Host's port, should be changed, depending on what is open and being used.
