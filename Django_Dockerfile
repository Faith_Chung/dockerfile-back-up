###################################################################
# Note: This file should be placed in the same directory as the   #
#       manage.py file.                                           #
# --------------------------------------------------------------- #
# This base image (python 2.7) is needed to install MySQL-python, #
# and utilize pip install. This is an official Python runtime     #
# parent image.                                                   #
###################################################################
FROM python:2.7

###################################################################
# Set the working directory to /app. Command lines will execute   #
# in this directory IN THE DOCKER CONTAINER.                      #
###################################################################
WORKDIR /app

###################################################################
# Copies the current directory contents on the host, into the     #
# container at /app.                                              #
###################################################################
ADD . /app

###################################################################
# Install Django in Docker Container.                             #
###################################################################
RUN pip install django

###################################################################
# Install other modules. These dependencies are based on the      #
# requirements.txt file. However, these specific modules worked   #
# best to get Django working on a bare VM, compared to using a    #
# pip install over the entire requirements.txt account.           #
###################################################################
RUN pip install --upgrade setuptools
RUN pip install coverage
RUN pip install djangorestframework
RUN pip install --upgrade oauth2client
RUN pip install facebook-sdk
RUN pip install MySQL-python

###################################################################
# Make port 80 available to the world outside this container      #
###################################################################
EXPOSE 80

###################################################################
# Run app.py when the container launches                          #
###################################################################
ENTRYPOINT ["python", "manage.py"]
CMD ["runserver","0.0.0.0:80"]